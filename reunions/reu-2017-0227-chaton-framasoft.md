# Rencontre avec Pouhiou de framasoft (lun 27/02/17)

Présents : Pouhiou, Tom, Thomas, Manue

Tom expose l'idée de créer un chaton dédié aux associations, l'idée a germé parce qu'il est bénévole à Combustible et qu'ils se posaient beaucoup de question où héberger par exemple leurs vidéos.

Statut juridique ? plutôt une association.

Le PIC, projet Internet et Citoyenneté basée à Toulouse, propose des services de mailing liste et hébergement spip à ses adhérents, mais ses serveurs seraient hébergés par OVH.

Tetaneutral pourrait être partenaire, par exemple en nous prêtant du moins au départ une ou deux machines virtuelles. Les sympathisants de Toulibre pourraient être intéressés pour contribuer. Combustible serait plutôt client.

Par ailleurs, Framasoft est partant pour participer à Alternatiba, selon les disponibilité puisque c'est la période de communication autour de Degooglisons.fr, mais dans tous les cas le matériel de communication pourra être mis à dispo.

Le processus d'intégration dans le collectif des chatons peut prendre un peu de temps : la 2ème portée est en attente depuis octobre, et les discussions prennent souvent du temps. Ouvaton est par exemple en attente, car historiquement il reste des outils propriétaires dans leur solution d'hébergement.

Quelques précautions rappelées par Pouhiou :

* Ne pas oublier l'**aspect "intermediation"**: support, FAQ, initiations et formations aux usagers, et éviter que la relation avec les usagers devienne une relation client, il faut leur permettre d'être contributeur du projet.

* Sur le support, regrouper les questions qui reviennent souvent avec une FAQ, ne proposer qu'un seul point d'entrée pour les contacts, avec plusieurs choix.

* Bien **définir notre identité visuelle**, de manière à ne pas être confondu avec Framasoft

* La **disponibilité des ressources bénévoles peut varier**, penser à des solutions techniques simples à prendre en main pour de nouveaux bénévoles

* Bien séparer les associations les unes des autres au niveau de l'infrastructure

* Expliciter ce qui nous paraît évident

* Aspects juridiques : CGU, charte avec des versions résumées

* Sur la modération, chez Framasoft les propos hors-la-loi sont supprimés. La question se pose des logs, et de combien de temps on doit les garder, selon le pays où est situé le serveur (en Allemagne dans le cas de Framasoft)

Les premiers services :

* Framadate,
* Mypads
* Gestion d'adhérent → Galette
* Nextcloud avec agenda / contacts / fichiers

Alternatiba Toulouse a fait pour le moment le choix d'utiliser des google docs, on essaierait bien de leur proposer des alternatives. Il y a aussi des outils pour remplacer Trello, outil de liste de tâches amélioré (kanban), de plus en plus utilisé par les associations.

Pour la question du financement, les modèles sont très nombreux : cagnotte, adhésion, prix libre avec prix conseillé, participation libre pour les plus petites associations et prix fixé pour les plus grosses, coût divisé par le nombre d'usagers  … c'est à nous de choisir. Dans la charte l'idée c'est simplement qu'il ne faut pas devenir trop gros, et de ne pas faire du prix une barrière.

Exemples :

* [Infini](https://chatons.org/chaton/infini) pour le côté technique, les noms
* [OpenTunisia](http://opentunisia.org/) est le framasoft tunisien
* [leprettefr](https://chatons.org/chaton/leprettefr) est un chaton familial qui divise les coûts entre mes membres de sa famille

Le collectif des chatons n'a pas de préconisation sur la forme du chaton, mais peut proposer son aide sur des questions juridiques. Benjamin Jean et Lionel Maurel font partie du comité juridique, Lionel a d'ailleurs aidé Framasoft à relier les CGU (qui protègent Framasoft) avec la charte (qui protège les membres du collectif et leurs usagers).

Le nom ? Thomas a pensé à Occisoft : occipad, occidate, occicloud… ou bien pad.occisoft.net… Attention on peut se lasser du nom à devoir trouver des déclinaisons :) ("qui a le framascotch ?")

**Conclusion : beaucoup de chatons pensent à la technique avant tout, mais il faut aussi penser au support, à l'animation de la communauté des utilisateurs, et avoir une unité visuelle.**
