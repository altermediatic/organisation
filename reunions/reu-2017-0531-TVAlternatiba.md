# Réunion TV Alternatiba 31 mai 2017 au CASC

Ecriture emission : jeudi 22 juin à 15h

## programmation

Contraintes :

Emissions de 45' max : évite les lenteurs.

format : 4x45' plateau + 4x15' de diffusion = 4h de plateau.

Plateaux ouverts : animation possible par TV Bruits et / ou FMR, différentes formes : interview, débat, reportage pré-enregistré, autre idée originale

Diffusion en streaming, étude en cours pour autre que YTube, accès internet contacts en cours avec Tetaneutral.

### Propositions

Education aux médias
Interview de personnalité sur un sujet sonné.
Duplex avec Figeac

#### Des villages

* Habitat, demande de caméra au poing
* transport : débat UTAT & PDU (?)
* Economie : à préciser
* autres suggestions de Sylvie : agriculture & alimentation, institutionnels, climat/énergie/biodiversité

### Journée 1

 * Engagement 14h-14h45
 * plateau ouvert aux villages 15h-15h45
 * Education aux médias 16h-16h45
 * plateau ouvert 17h-18h

### Journée 2

 * plateau ouvert aux villages 13h-13h45
 * Transversalité 14h-14h45
 * plateau ouvert aux villages 14h-14h45
 * Debrief duplex avec figeac 16h à 17h

## À contacter

* Médias Cité
* Inform'action (attention, tout n'est pas net)
* Bad Kids
* IATAA
* demosphere

## Proposition de mail pour les villages

 Comme vous le savez ou pas (LISEZ LES COMPTES-RENDUS !!!) Alternatiba sera doté d'un plateau télé ! Grâce à TV Bruits,
 un espace d'expression alternatif et diffus sera présent !
 Sur le week end, chaque jour de 14h à 18h, il est possible d'utiliser ce plateau. Plusieurs formes ont étés réfléchies :
 
 - un plateau débat avec invités de 45'
 - un plateau interview de 15'
 - un reportage que vous avez dans votre valise à reportages
 - une démo d'un truc que vous avez dans votre stand
 - 15min "caméra au poing"

 Attention attention : rien n'est imposé ! Sentez-vous libre de proposer un format, un contenu !

 Une deadline parce que sinon on ne s'en sort pas : 26 juin !
 Envoyez vos propositions à toulouse_tv_alternatiba@alternatiba.eu !

## Implantation

Scène : plutôt praticable pour ne pas être trop en hauteur / public. Possibilité d'installer une toile au-dessus de la scène

barnum pour la régie : idée de caravane, ou un chalet de Bois & companie : à demander si c'est possible

Stockage : dans un chalet fermé, de toutes façons du monde va dormir sur place

