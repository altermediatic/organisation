# Réunion village AlterMediaTIC mardi 07/02/17

**Présents :** Augustin, Clément, Emmanuelle, Laurent, Youssef, Tom, Thomas, Philippe

## Retours sur la journée géniale du 22/01

## Infos

* mercredi 15/02 : Aurélien viendra le mercredi 15 février au TetaLab, de Bordeaux pour discuter de retours d'expérience en Télécoms-Réseaux, [montage de réseau de première nécessité](https://chiliproject.tetaneutral.net/projects/tetaneutral/wiki/R%C3%A9seau%20T%C3%A9l%C3%A9com%20de%20Premi%C3%A8re%20N%C3%A9cessit%C3%A9)
* plénière mardi 21/02 20h30 → Clément est disponible pour y assister
* Adrien de [Pick-a-Joy](http://www.pick-a-joy.com/), sera sur Toulouse samedi pour discuter avec les personnes disponibles

## Outils communication et collaboratifs

Présentation de MyPads à Laurent pour qu'il le présente à son tour à la coordination.

Un espace sur Framateam a été ouvert pour [Alternatiba toulouse](https://framateam.org/alternatibatlse/), et un salon est dédié au village [AlterMediaTIC](https://framateam.org/alternatibatlse/channels/altermediatic). Un salon avait été créé sur irc, mais n'a pas eu de succès.

Pour info, un [groupe alternatiba a été créé sur framagit](https://framagit.org/altermediatic/) pour gérer les dépôt de code, un premier [dépôt héberge notre documentation](https://framagit.org/altermediatic/organisation/tree/master) et les "issues" (tickets).

Une [page AlterMediaTIC](https://alternatiba.eu/toulouse/villages-thematiques/numerique-et-media/) est créé sur le site, et il est possible de créer des sous-pages. Manue et Laurent ont les accès.

## Projets pour Alternatiba 2017

### Ateliers analyse critique de media

Pas mal d'idées, mais besoin des personnes qui étaient présente à l'atelier pendant la journée géniale, et qui sont bien au point sur les media.

### Plateau Télé avec TV Bruits

Voir avec Roberto et les autres de TV Bruits

### Hébergeurs alternatifs (CHATONS)

Deux aspects :

* présentation des CHATONS (bien sûr)
* création d'un hébergeur CHATON, à destination des associations

### Création d'un CHATON

**Équipe :** Tom, Thomas, Manue, Augustin, une autre personne intéressée

Existence du PIC, Projet Internet Citoyen, mais n'envisagent pas de se lancer dans la construction d'un CHATON, car pas assez de ressources bénévoles. Leurs services sont peut-être hébergés chez OVH (à confirmer).

Par contre, le public principal du PIC sont les associations, ça rejoint notre objectif. Peut-être qu'on peut proposer notre aide technique pour mettre en place un hébergeur chaton.

Opportun d'aller vers une structure existante ? Ça dépend, il faut les rencontrer.

TODO

* contacter le PIC
* contacter Pouhiou

La discussion continue sur ce que pourrait être cet hébergeur toulousain.

Quels services proposer en premier ?

* chat (mattermost, rocket chat)
* partage de document (own/nextcloud)
* framadate
* pad / wiki ou evernote like
* discussion video jitsi / mumble

Comment financer le service ? gratuit ? adhésion ? PLN (participation libre mais nécessaire) ? selon le nombre de salariés ?

La question des écoles : la crainte est de ne pas pouvoir offrir un service d'une qualité suffisante par rapport à l'exigence des écoles. À savoir que les services métiers sont déjà fournis par les académies, les écoles pourraient très bien utiliser les services du CHATON.

Info : une entreprise a fait il y a peu un sondage auprès des associations et leurs usages du numérique, mais elle ne propose que des services propriétaires utilisant des outils de Windows.

### Hackaton

**Référent :** Clément

Le projet : inviter des personnes pour réaliser un projet geek sur le thème de la transition (exemple de farmbot). Ça peut être la contribution à des projets existant.

Concrètement ça se traduirait par espace à disposition avec électricité, internet et matériel. Sûrement besoin de trouver des partenaires associatifs ou autres pour y mettre un peu de budget.

Le weekend peut être assez agité, et pas forcément propice à réfléchir sur des projets : on pense surtout à faire des "proof of concept", pas des produits finis. Il faudra gérer l'accueil pour installer les contributeurs⋅trices.

L'idée est de répondre à des besoins d'acteurs d'alternatives.

Pas de récompense prévue, sauf de parler des projets sur le futur plateau de TVBruits.

Les besoins : 1 tente, l'électricité, un accès internet

Clément défriche le projet, et sera à la plénière s'il faut le présenter.

### Accès internet sur le site

Demande de la commission technique d'avoir un accès internet sur la prairie des filtres.

Clément souhaite explorer le projet commutation wireless. Ce projet permet de créer rapidement une infrastructure réseau avec quelques routeurs TPLink, et utilise tous les équipement sans fil de la zone pour créer un wifi maillé. Mais ce n'est pas certain que tout fonctionne.

Le 15/02 au Tetalab est organisée une rencontre avec une personne ayant déjà monté des ateliers autour des réseaux de première nécessité (cf mail de Philippe), Clément a prévu d'y aller.

Ça pourrait être aussi l'occasion de proposer un atelier au THSF, et y parler d'alternatiba.

En backup, on peut demander à tetaneutral de nous aider.

### Pick a Joy, réseau social P2P et chiffré

Squeeeks reçoit Adrien, contributeur du projet PickAJoy, réseau social P2P chiffré. Pourrait être proposé en conférence ou en débat, aussi bien au THSF pour le côté technique qu'à alternatiba pour parler des enjeux de la décentralisation et du chiffrement.

### La quadrature ?

À propos de surveillance et vie privée, on peut aussi proposer à la quadrature d'intervenir : quadrapéro, atelier ou conférence ? Info sur un deuxième atelier à Paris sur le droit d'auteur, intéressant pour le village culture.

### Impacts écologiques

[Dossier de Greenpeace sur l'impact écologique des datacenters](http://www.clickclean.org/downloads/ClickClean2016%20HiRes.pdf), [site internet dédié](http://www.clickclean.org/france/fr/). Attention à JerryDIT, intéressant pour l'aspect éducatif, mais à l'échelle n'est pas forcément plus écologique, car utilise des jerrican alors qu'il reste souvent beaucoup de boitiers UC non utilisés.

Remettre les panneaux au goût du jour, ils sont pour le moment stockés chez Laurent.

### Atelier auto-défense numérique

**Équipe :** Clément et Augustin

Proposer des éléments basiques pour protéger sa vie privée sur internet.

Exemples d'atelier :

* sensibiliser au chiffrement
* fonctionnement d'internet
* atelier programmation sans ordinateur [CS Unplugged](http://csunplugged.org/books/) et [en français](https://interstices.info/jcms/c_47072/enseigner-et-apprendre-les-sciences-informatiques-a-l-ecole)

Mais aussi

* panneau explication informatique, glossaire des termes utilisés dans le village
