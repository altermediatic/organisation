# Réunion village AlterMediaTIC lundi 10/04/17

**Présents :** Manue, Nim, Tom, Squeeek, Philippe

## Journée du 22/04, lancement Alternatiba

Dispos : Manue et Squeeek, mais peu de temps pour préparer en amont.

On prévoit juste un atelier sur un ou plusieurs sujets parmi les projets en cours et choisis par les nouveaux venus. Récupérer les panneaux de 2015 chez Laurent.

L'idée de défilé ne plaît à personne dans le village, gros risque d'avoir des préjugés sur les hackers.

Manue essaie d'aller à la réu de prépa.

## Communiquer aux assos potes

Page internet ? oui, sur le [site Alternatiba](https://alternatiba.eu/toulouse/villages-thematiques/altermediatic/), à mettre à jour

**mise à jour page web → Manue - 14/04**

Ensuite on se répartit la comm' par la mailing liste.

**Envoyer mails aux assos potes → tous - 15-16/04**

Guilhem fait partie de Caracole, a vu passer les échanges à propos de Alternatiba, donc connais le principe.

## Projet Chatons

### Rencontre avec le PIC

Tom a rencontre le PIC à leur réu hebdo du CA mardi 28/03 (6 membres du PIC présents). Accueillent le projets favorablement, avaient envie de faire de même depuis longtemps mais pas assez de ressources bénévoles.

Deux modalités possibles :

1. chacun fait son association et on fait un partenariat croisé (chaque asso renvoie vers l'autre selon les demandes)

2. le projet rentre dans le cadre de l'asso du PIC, mais technologiquement on fait un serveur vierge

  Avantages :

  * pas d'embêtements administratifs
  * bases d'adhérents qui pourraient bénéficier tout de suite des nouveaux services

  Inconvénients :

  * peu de moyens propres au PIC
  * technos maîtrisées assez anciennes par rapport à notre projet
  * réactivité assez lente

Vérifier si le fonctionnement du PIC est ok avec la charte des chatons.

Dans les deux cas, Tetaneutral est prêt à avancer une ou deux VM.

Parmi les présents, plutôt partants pour la solution 2. Le PIC réfléchit également de son côté, on se revoit dans l'été pour rediscuter de l'avancement du projet.

Serait aussi intéressant de leur poposer un atelier sur les technos Docker, et voir si des assos adhérentes seraient motivées pour faire beta testeur.

### Aspects techniques

Tout le monde partant pour CoreOS et Docker.

**Programmer un atelier Docker : avec Toulibre ?**

Lionel et Pierre F. sont très calés sur Docker, animateurs du meetup DevOps, et viennent de créer un meetup Docker. Des vidéos sont disponibles pour commencer.

Objectif supplémentaire : partager des recettes docker, ce que n'arrivent pas Framasoft et d'autres chatons car trouvent leur code "trop crade".

Question sur un installateur facilité type Yunohost ? Dans une deuxième phase.

SSO ? risque sécu pour Framasoft qui ne l'implémente pas dans ses services. Deuxième phase aussi si besoin.

### Prochaines étapes

* créer 2 VM test et prod → Tom
* programmer un atelier Docker
* étudier l'install de Cryptopad (type 0bin)
* créer un compte liberapay → Manue
* donner accès [git Framagit](https://framagit.org/altermediatic)
* trouver un ndd → Manue chez gandi

Nom ? occita.net est pris :(

## Autres projets

### Accès internet

Les investigations continuent sur Commotion Wireless. Problème, le projet même s'il parait bien fait est non-documenté. Aucun PoC documenté n'existe sur le net. Des crash dans le daemon, des SIGSEV,... il y a un gros boulot à faire. 

Accès internet en mode dégradé intéresserait potentiellement la croix rouge dont fait partie également Guilhem.

=> Contact avec Tetaneutral pour la mise en place d'une connexion sur la prairie parce que Commotion ne sera pas prêt. Cependant, Clément va continuer à bosser dessus :
- debugging
- Mise en place d'un git local avec un build system correct (sur le CHATON ou sur mon serveur)
Communiquer autour pour ramener du monde intéressé pour bosser dessus => Clément débroussaille le sujet pour expliquer l'architecture.

### Hackathon

Règles de participation/rétribution terminée.
Mail d'invitation terminé, plus qu'à envoyer à des assos. 
TAF : logistique à gérer en termes d'espace, élec', matériel,...

### Autodéfense numérique

Pas d'avancées. Augustin et Clément doivent se capter pour définir l'atelier.

### Greenpeace. 

Invitation lancée : pas de réponse, passage en mode spam !

### Critique des media

**assos à contacter : Inform'action, friture → Manue**

Inviter des youtubeurs comme Hygiène mentale ou Hacking social (présents au THSF 2016) ? pourquoi pas mais pas de budget déplacement.

Proposition pour la communication : un minitel pour récupérer les adresses mails des personnes intéressées

### PickAJoy

Info : dev en pause car manque de moyens. Projet en recherche de contributeurs.

**Relancer le village conscience → Squeeek**
