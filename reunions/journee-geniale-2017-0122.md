# Journée Géniale Alternatiba 22/01/2017

Autour de la table
Manue / numahell : vient de toulibre, développeuse web, mouvement des communs à Toulouse
Roberto : TV Bruits
Raph : Graphisme, vidéo
Clément
Augustin : OpenstreetMap, OpenData, intéressé par la création de CHATONS
Tom' : Développeur, intéressé par la création de CHATONS.
Toto : Etudiant informatique.

## Médias

Tenter de réaliser un plateau sur Alternatiba. Créer une synergie autour des développeurs pour la mise en place dudit plateau.

Besoin aide technique pour accès au réseau en situation de faible connectivité.

Liste des médias indés :

* Zoom Verts
* TV Bruits.
* Fil Rouge (réalisation de docus, travaille beaucoup dans les quartiers).

Gazettes :

* Beugle
* Friture

### Objectifs

* traiter des sujets de manière différente que approche dominante
* alternative sur
    * techniques utilisées
    * audience
    * sujets plus poussés/consistants
    * participation citoyenne/diversifier les intervenants par rapport au medias dominants
* comment avoir une audience (sensibilisation au grand public)
* quelle forme d'atelier ? pas forcement atelier tech (compliqué à gérer) mais plutôt ouvrir un plateau. Recherche de thématiques transverses pour lier media et numérique, par exemple en faisant le lien entre le contenu à diffuser et les outils libres de diffusion

Considérer le numérique comme le socle de diffusion des médias alternatifs.

Travail sur l'esprit critique du lecteur, différencier media libre/media limite-conspirationniste

* Exemple de Clément sur l'idée d'un guide d'autodéfense vis-à-vis des pseudo-sciences etc. Réfléchir aux éléments de langages pour contrer les trolls/phrases faciles
Ateliers anti-désinformation ?

## Numerique

[Les objectifs de 2015](https://alternatiba.eu/toulouse/villages-thematiques/numerique-et-media/altermediatic-2015/#obj)

* **Se degafamiser au quotidien**
    * Le numérique dans le travail, quels outils ?
    * Comment monter rapidement un serveur complet ?
    * Outils collaboratifs : types Slack

* **Chaton toulousain**
     * approche Tom
         * plutôt pensé pour les assos/collectifs et l'organisation d'événements, plutôt que pour les particuliers. Eviter le pro dans un premier temps.
         * mise en place d'un site de communication.
         * reflexion autour du modele economique (participation libre, contributions)

# AlterMédiaTic (Médias alternatifs + numérique)

## Objectif de fond

* <sup>Montrer les alternatives en termes de médias et numérique</sup>
* <sup>Proposer une alternative citoyenne à la crise du numérique</sup>
* **Augmenter les pratiques et usages des medias et outils numeriques alternatifs**

## Organisation

2 référents + description des formes de travail
* manue
* clément

Réunions mensuelles dans un bar de la ville.

## 3 sous-thématiques

Peut-être à réintituler

* media alternatifs et citoyenneté numérique
* logiciels libres, alternatives GAFAM (Google, Apple, Facebook, Amazon, Microsoft)
* impact écologique

## 3 modalités pratiques pendant l'événement

* Ateliers
* Présentations
* Conférences
* + ressources en ligne

## Liens avec d'autres thématiques

Des liens possibles avec les autres thématiques dont un concret avec l'une d'entre elles

* déchet et emploi
* santé
* énergie
* démocratie / citoyenneté

en plus après atelier

* éducation
* culture
* juridique

## Effet souhaité au niveau de la ville et la région

* hébergeur "Chatons"
* carto quartier OpenStreetMap

## les besoins en logistiques

* Multiprises,
* Electricité
* Tentes
* Ordinateurs
* Vidéo projecteur
* Tables + chaises
* Internet si possible
* briques internet ?

## Liste des assos

À compléter

* **Médias**
    * Zoom Verts
    * TV Bruits.
    * FMR
    * Canal Sud
    * Fil Rouge (réalisation de docus, travaille beaucoup dans les quartiers).
    * Gazettes :
        * Beugle
        * Friture
* **Numérique**
    * framasoft,
    * mozilla,
    * OSM Toulouse
    * plus local : tetaneutral.net toulibre …

## TODO

* inscrire tout le monde à la liste de diffusion
* chan IRC #mediatic
* [contenu de la journée](https://mensuel.framapad.org/p/2XE7iEAE2C)
