# Les projets pour Alternatiba 2017

## Création d'un hébergeur CHATONS à destination des associations

Créer un hébergeur alternatif répondant à la [charte des CHATONS](https://chatons.org/charte-et-manifeste) proposée par Framasoft, et proposant des services à destination des associations.

**Équipe :** Tom, Thomas, Manue, Augustin, une autre personne intéressée

[En détail](chatons-toulousain.md)

## Hackaton

Construire pendant le weekend d'alternatiba un projet geek sur le thème de la transition citoyenne.

**Équipe :** Clément

## Accès internet sur le site

Mettre en place un accès internet sur le site d'Alternatiba

**Équipe :** Clément

## Ateliers analyse critique de media

Proposer des éléments basiques pour aiguiser son esprit critique sur les media.

→ Des idées, sollicité asso-icare sur les conseils de Raf, projet de faire un atelier en commun avec le village Alternatives éducatives.

**Équipe :** Clément, Manue

[En détail](education_aux_media.md)

## Plateau Télé avec TV Bruits

Mettre en place un plateau TV pendant le weekend Alternatiba

→ voir avec TV Bruits

## Atelier auto-défense numérique

Proposer des éléments basiques pour protéger sa vie privée sur internet.

**Équipe :** Clément, Augustin

## Présentation d'alternatives libres

* Framasoft
* la quadrature
* Pick a Joy (→ Squeeeks)
* …

[Mix des deux projets](Reprenez_la_main_sur_votre_vie_numerique.md)

## Impacts écologiques du numérique

Montrer les impacts écologiques (et sociaux ?) du numérique.

**Équipe :** Manue
