# Création d'un hébergeur CHATONS toulousain

Créer un hébergeur alternatif répondant à la [charte des CHATONS](https://chatons.org/charte-et-manifeste) proposée par Framasoft, et proposant des services à destination des associations.

## Quelques questions qu'on se pose

### Quels services proposer en premier ?

* chat (mattermost, rocket chat)
* partage de document (own/nextcloud)
* framadate
* pad / wiki ou evernote like
* discussion video jitsi / mumble

> *Tom :* Pour une première itération de test, je pencherais pour un équivalent framapad + framadate (autre outil simple et aussi assez utile pour des assos). On verra la suite en fonction des bonnes volontés disponibles (et des envies) de chacun.
> L'idéal serait de rapidement pouvoir le "dogfooder" en l'utilisant pour le travail sur le chaton lui-même :D Du coup ces deux outils me paraissent pertinents.

### Comment financer le service ?

Gratuit ? adhésion ? PLN (participation libre mais nécessaire) ? selon le nombre de salariés ?

## L'existant à Toulouse

## Les étapes

- [x] Contacter Framasoft
- [x] Rencontrer Framasoft → [CR de la rencontre](../reunions/reu-2017-0227-chaton-framasoft.md)
- [ ] Contacter le PIC
- [ ] Contacter tetaneutral pour le coût de l'hébergement de VM
- [ ] Trouver un nom au chaton
- [ ] Tester les outils utilisés par framasoft (voir le [wiki des CHATONS](http://wiki.chatons.org/))
- [ ] Demander à tetaneutral 2 VM
