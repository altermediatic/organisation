Proposition à retrouver et discuter sur https://mypads.framapad.org/mypads/?/mypads/group/altermediatic-toulouse-deatm79d/pad/view/reprenez-la-main-sur-votre-vie-numerique-1z6qk773

# Atelier - Reprenez la main sur votre vie numérique
**Ce pad centralise les propositions de contenu pour l'Atelier "Reprenez la main sur vore vie numérique"**

**Proposition** : il était prévu 2 ateliers différentiés "Alternatives libres" et "Autodéfense numérique", ce pad propose de les réunir sour forme d'un seul atelier "Reprenez la main sur vore vie numérique", titre repris d'une tribune de 2015 https://alternatiba.eu/toulouse/2015/08/10/reprenez-la-main-sur-votre-vie-numerique/. En effet les 2 sont imbriqués et il peut être intéressant de les réunir.

## Notes

* rappel ojectif principal AlterMediaTic : Augmenter les pratiques et usages des medias et outils numeriques alternatifs
* il s'agit d'une proposition de base qui doit être discutée/réorganisée/diminuée

## Todo

* compléter/valider les thématiques et leurs contenu
* définir format des supports de com pour chaque thématique retenue
* prévoir un moyen de transmettre des ressources avant/pendant/après l'événement

## Approche générale

* Proposer un parcours thématique à suivre "de bout en bout", seul ou accompagné
* Prévoir en parallèle des thématiques à la carte
* On pourrait imaginer un support de com (panneau ou autre) pour chaque thématique mais ça risque de faire beaucoup

## Thématiques
### De bout en bout

* Etat des lieux de la surveillance
* Des concepts clés pour trouver des alternatives
* Des alternatives et un accompagnement libres au quotidien
* Autodéfense numérique en pratique

### A la carte

* Des mots clés en vrac pour ouvrir des discussions/explications
* Expliquer certains concepts en pratique
* Ressources

## Détail des thématiques
### Etat des lieux de la surveillance

* pourquoi ?
   * modèle économique du profilage et de la publicité ciblée
   * contrôle de la population
   * espionnage étatique/industriel
* comment ?
   * centralisation / GAFAM
   * outils propriétaires ( verrouillés et inconnus )
   * traces à tous les étages
   * smartphones, objets connectés (compteurs connectés,...)
   * cloud
   * gratuité/freemium
* quels dangers ?
   * qu'est ce que Facebook ou Google savent de vous ? (idée : aperçu sous forme de tableau de la variété des infos
   * collectées par les X Google services + trouver un aperçu du fonctionnement de Google Analytics.
   * Vault 7 : récupérer 2 ou 3 exemples pour illustrer les moyens militaires de surveillance et cyberguerre ?
   * dangers personnels et pour nos proches / dangers pour la société
   * et la loi dans tout ça ? Montrer l'inadéquation temporelle.
* l'illusion du "j'ai rien à cacher"-> à merger avec le 1er danger.


### Des concepts clés pour trouver des alternatives

* matériels/logiciels libres communautaires respectueux de la vie privée : expliquer les concepts d'open hardware et
* open source et en quoi ils répondent au besoin de transparence
* décentralisation et contrôle du serveur -> projet CHATONS, information sur l'auto-hébergement (http://www.bortzmeyer.org/presence-en-ligne.html )
* chiffrement bout à bout
* réflexions sur le modèle économique (modulé par le niveau de vie) : pourquoi parler de modèle économique ? Facture
* électrique ? rémunération des acteurs ? Quid de la démarche associative ?
* Réduction de la fracture numérique : éducation au numérique.

### Des alternatives et un accompagnement libres au quotidien

* Framasoft (dégooglisons internet)
* Chatons
* Brique Internet / Yunohost
* Tetaneutral
* Toulibre
* La Quadrature
* Combustible


### Autodéfense numérique en pratique

Note : intégrer l'autodéfense sur smartphone. Attention le smartphone est un système ultra fermé. Quelques alternatives existent à partir d'AOSP http://www.replicant.us, cyanogenmod mais le problème principal reste les firmwares propriétaires dans le smartphones.

* les questions à se poser quand on doit choisir un nouveau service (cf "2. Des concepts clés pour trouver des alternatives")
* en pratique
   * OS : le point qui fâche, bien sûr ne pas en faire un prérequis : il est possible d'extraire quelques points
   * clés pour aider à choisir un OS. Cela dépend des termes : performances ? Concepts de base d'un système ? Vie
   * privées ? Suites logicielles ? Modèles économiques ?
   * Fournisseur d'accès à internet ( French Data Network )
   * Mozilla Firefox (agrandir à browsers en général)
       * choisir ses (méta)-moteurs de recherches : DuckDuckGo, Qwant, Searx, StartPage/Ixquick ... attention aux effets de mode Lilo etc.
       * gestion cookies
       * historique
       * Master Password
       * choisir ses extensions : Privacy Badger, uBlock Origin, HTTPS EveryWhere, NoScript, uMatrix
   * Phrase de passe
       * définir une politique de gestion de phrases de passe
       * utiliser un gestionnaire KeePass2
   * Tor Browser Bundle
   * Choisir son fournisseur courriel : service militant vs service pro / gratuit vs payant ("il n'y a que dans les pièges à rat que le fromage est gratuit")
   * Chiffrement symétrique/assymétrique
       * support physique
       * courriel
       * Jitsi ou Pidgin (chat via XMPP/OTR + VoIP via SRTP avec Jitsi)
   * Tails (est un OS)

Note clément : Tous ces item concernent le chiffrement, la confidentialité mais ne s'oriente pas forcément sur la question du libre, d'un modèle alternatif. Auto-défense numérique ne veut pas seulement dire "anonymat et confidentialité" mais aussi sortir d'un modèle de trust économique, sortir d'un étranglement technologique,…

Bien que les questions de confidentialité sont cruciales elles sont aussi permise par les GAFAM, la standardisation, les codes fermés non soumis à la révision par les pairs.

### Des mots clés en vrac pour ouvrir des discussions/explications

Note : à classer par catégories

* (dé)centralisation
* cryptographie as(symétrique)
* SSL/TLS
* GPG
* XMPP/OTR
* Tor
* matériel/logiciels libres/propriétaires
* consentement explicite
* intérêt légitime
* profilage
* droit à l'oubli
* droit à la portabilité des données
* fuite des donnée
* données à caractère personnel
* GAFAM
* big data
* vie privée
* surveillance
* Tails
* backdoor
* cloud
* objets connectés
* quantified self
* beacons
* Xkeyscore, Prism etc.
* identités contextuelles

### Expliquer certains concepts en pratique

Note : approche CS Unplugged

* cookies
* empreinte navigateur
* chiffrement point à point (symétrique/assymétrique), appliqué à SSL/TLS, GPG,
* routage en oignon
* code libre vs code fermé (pourquoi RSA est le meilleur algo de crypto ?)
* la centralisation des services et données.
* comprendre les briques constituant un OS, un ordinateur pour mieux comprendre les sujets liés.


### Ressources

* https://controle-tes-donnees.net/
* https://degooglisons-internet.org/
* https://mediakit.laquadrature.net/
* https://guide.boum.org/
* https://prism-break.org/fr/
* https://emailselfdefense.fsf.org/fr/
* https://www.torproject.org/
* livre surveillance:// de T.Nitot
* https://www.cnil.fr/fr/
