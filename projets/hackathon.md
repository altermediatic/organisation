# Hackathon

Le but de ce projet est de fournir un espace de refléxion collective autour du sujet suivant : _En quoi le numérique
peut servir la transition citoyenne ?_

## Règles

Le hackathon n'est pas pensé comme son pendant marketing et industriel. Ici le but est de faire émerger des projets
utiles. Ainsi les règles suivantes sont fixées :
* Nombre maximum de projets : 5 - 10
* Nombre de personnes/projet : on s'en fout
* 8h pour pondre une conception, une architecture, un synopsys exploitable
* Pas de gagnants : Les projets aboutis bénéficieront de la puissance de communication d'Alternatiba.
* Une aide au matériel peut-être fournie aux participants


## TODO List

* Voir avec la com' comment utiliser leurs supports pour envoyer des invitation dans les milieux hackers, makers,....

### A contacter
* Toulibre
* Tetalab
* Combustible
* Open Bidouille Camp
* ...
