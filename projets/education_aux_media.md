# Éducation aux media

[Pad brainstorm](https://mypads.framapad.org/mypads/?/mypads/group/altermediatic-toulouse-deatm79d/pad/view/education-aux-media-99ty71e)

## Objectif

* transmettre des éléments permettant d'avoir un regard critique sur l'information et les media
Pour qui :
* tout public, adultes et ados

## Contenu

* ressources et supports
* débat, forum ouvert
* ateliers

### Émission TV / forum ouvert "Quelle éducation aux media aujourd'hui ?"

*Proposition de texte de présentation :*

"Fake news", "Hoax", théories du complot, viralité dans les réseaux sociaux, manipulation, cadrage ou hiérarchie de l'information. L'élection de Trump a cristallisé la nécessité d'éduquer aux media, les jeunes comme les adultes. Des initiatives existent, dans l'éducation nationale et dans l'éducation populaire, mais également de la part de medias eux-mêmes. Aujourd'hui la question se pose : “Quelle éducation aux media en 2017 ?”

### Atelier décryptage d'un jeu vidéo

Les jeux vidéos, amener à la prise de conscience de la manipulation. Les dessous des éditeurs d'appli. Gratuit ? quel est le modèle économique ?

* Clément, Julien
* réu 14 juin chez Clem pour préparer cet atelier

### À valider

* table de presse Inform'action
* exposition "Le Saviez-vous ?" / Inform'action
* atelier 45min-1h : "Médias et démocratie : une interdépendance" / Inform'action

### Autres idées

Ces idées proviennent de nos réflexions et discussions, nous les gardons ici pour mémoire.

#### Débats : (pleins d'idées)

* qui peut juger de la qualité de l'information ?
* les media indépendants sont-ils plus fiables ?
* le traitement de l'écologie / altermondialisme dans les media
* créer son media, autonomisation des publics
* les oubliés des media : habitants des quartiers populaires, zones rurales, les femmes, les personnes racisées

#### Ateliers

* prendre une information, analyser comment elle a été traitée par différents media
* vérifier l'information : la chasse aux hoax et aux mensonges, "alt facts"
* déconstruction mentale, ou se pencher sur ses propres préjugés

### Supports possibles

* les media et leurs propriétaires
* les clés de décryptages :
    * type de media (journal, blog, réseau social, vidéo, podcast…
    * type de contenu (article, billet de blog, édito, tribune, commentaire …)
    * savoir qui écrit, d'où vient-il-elle sociologiquement parlant
    * sociologie des media : fonction d'agenda, filtres, fonction de cadrage, …

## Ressources

### Exemples d'ateliers

* Animer un [atelier de désintoxication de la langue de bois](https://www.youtube.com/watch?v=8oSIq5mxhv8) : la SCOP sur www.lecontrepied.org
* Déconstruction d'un pure player
* Comment se propager une fausse information : un jeu dont vous êtes le héros


### Medias spécialisés

* [Acrimed](http://www.acrimed.org/), observatoire des medias
* [Arrêt sur Image](http://www.arretsurimages.net/), réfléxion critique sur les media, articles et une émission vidéo hebdomadaire.

### Organes de vérification par des media

* [Les Décodeurs, le Monde](www.lemonde.fr/les-decodeurs/)
* [Désintox, Libération](http://www.liberation.fr/desintox,99721)
* [Le vrai du faux, France Info](http://www.francetvinfo.fr/replay-radio/le-vrai-du-faux/)

### Émissions radio

* [L'atelier des media](http://www.rfi.fr/emission/atelier-medias) sur RFI

### Associations et orgas locales

* [Asso Icare](http://www.asso-icare.org/)
<<<<<<< HEAD


### Réunion atelier éducation aux médias avec TV Bruits.

Dans l'Education, l'éducation aux médias était drivée par un motto : "c'est mal". Aujourd'hui avec internet et réseaux
sociaux, les médias pénetrent l'école et les milieux de l'éducation or le motto ne change pas vraiment :
=> quel impact actuellement ? quels moyens ?
La consommation massive des médias à déconstruire.

Réunir des acteurs de l'éducation autour des moyens de l'éducation aux médias.
Atelier décryptage : TV Bruits à construit une "borne de décryptage" à partir de frigo. Quel public ?
Asso faisant des supports aidant au décryptage : cap'nomade, canopée, CLEMI, checker au CRDP (à côté de décat'), la
trame passeur d'images.

Réunir un parterre d'acteurs institutionnels, de terrain, associatifs, universitaires autour des questions de l'état de
l'art de l'éducation aux médias et de l'avenir de ce sujet. Y'a t'il convergence, divergence ?

Ateliers : internet et médias. Rien que ça !
           décryptage jeux vidéos sur smartphone. ZBRU ! 
           décryptage Télé.


=======
* [Inform'action](http://www.informaction.info/)
* Enseignants documentalistes au collège

### Medias indépendants

* Mediapart
* Les jours
* Brut
* Reporterre
* [Mediacités](https://www.mediacites.fr/)

### Portails de presse

* [Portail libre Bastamag](https://portail.bastamag.net/spip.php?page=sources)
* [Portail La presse libre](https://beta.lapresselibre.fr/)
>>>>>>> bc3c429f9ea86d5e6edef246450cc02f3cc92e9d
