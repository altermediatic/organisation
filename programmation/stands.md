# Stands

## Tonnelle 1 - logiciels libres

* Le Pic
* Framasoft

## Tonnelle 2 - internet libres

* Tetaneutral
* Pick-A-Joy (seulement samedi après-midi)

## Tonnelle 3 - relais medias libres

* Demosphere
* Inform'action

## Tonnelle 4 - media libres

* Onde courte
* TV Bruits
* FMR ?

## Tonnelle 5 - ateliers 1

voir programmation

## Tonnelle 6 - ateliers 2

* hackathon
* tetalab
