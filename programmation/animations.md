# Programmation

En cours de construction sur [ce pad](https://mypads.framapad.org/mypads/?/mypads/group/altermediatic-toulouse-deatm79d/pad/view/programmation-village-ae3s447vs).

## Samedi 23/09

### Atelier FAQ du PIC

village (espace atelier), 14h-18h (ou 17h ?)

### Émission "L'engagement citoyen"

plateau TV, 14h-14h45

### Émission "quelle éducation aux media ?"

plateau TV, 16h-16h45

### Conférence Pick-A-Joy

cœur village, 16h-17h

## Dimanche 24/09

### Atelier "Médias et démocratie : une interdépendance", par Inform'action

village (espace cœur), 11h

### Conférence Framasoft Dégooglisons, par Pouhiou (1h)

Grande tente conférences, 13h

### Émission "La transversalité"

plateau TV, 14h-14h45

### Atelier Framasoft : quelles alternatives aux GAFAM ?

village (espace atelier), 14h-16h

### Atelier décryptage jeu vidéos

village (espace atelier), 16h00 ? ou le matin ?

## À programmer

## Récup' et Linux, par Joyce

village, (espace cœur)
