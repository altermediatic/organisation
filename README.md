# Organisation du village AltermediaTIC Toulouse Alternatiba

Le village AlterMediaTIC souhaite mettre en valeur les alternatives dans les dommaines des media et du numérique. Ce dépôt est utilisé par le village AlterMediaTIC Toulouse pour y stocker sa documentation.

## Objectif du village pour Alternatiba 2017

**Augmenter les pratiques et usages des medias et outils numeriques alternatifs**

### Thématiques

* media alternatifs et citoyenneté numérique
* logiciels libres, alternatives GAFAM (Google, Apple, Facebook, Amazon, Microsoft)
* impact écologique

## Actions

* Ateliers
* Présentations
* Conférences
* + ressources en ligne
* Hackaton: construction en commun

## Liens utiles

[Liste des projets pour Alternatiba 2017](projets/)

[Compte-rendus des réunions du village](reunions/)

[Salon de discussion instantannée](https://framateam.org/alternatibatlse/channels/altermediatic)

[Pads](https://mypads.framapad.org/mypads/?/mypads/group/altermediatic-toulouse-deatm79d/view)
