# Atelier remue-méninges

Voir l'avancement sur le pad (https://mypads.framapad.org/mypads/?/mypads/group/altermediatic-toulouse-deatm79d/pad/view/espace-web-du-village-k55957m2)

# Page : Accueil

## Objectifs

Objectif principal

### Actions

## Projets

## Actualités / Évènements

### Alternatiba 2017

Teasing, les rendez-vous, la prog

### Alternatiba 2015

rappel du programme → page spécifique

# Page : Le numérique, enjeux et alternatives

les 5 volets, liens vers les posters
Indiquer les solutions et les alternatives locales
Si on trouve du temps, rédiger une page par volet ?

# Page : Le numérique dans les autres alternatiba

Lien vers les infos sur le projet de cartographie des alternatives
