# Logiciels libres / thématiques

Encyclopédie Ekopedia

La ressource pour trouver un logiciel : framalibre

## Agriculture et Alimentation

Cagette pour gérer une AMAP
Telabotanica pour la flore
OpenSource Ecology
l'Atelier paysans

## AlterMediaTIC

Réseaux sociaux numériques : mastodon, framasphère

[Collectif Emmabuntüs](https://framalibre.org/content/collectif-emmabunt%C3%BCs)

degooglisons internet

Projet Chatons : hébergeurs solidaires
FFDN : réseau de FAI associatifs
GULL : groupe d'utilisateurs de logiciels libres

Et bien sûr linux

## Alternatives Educatives

Wikipedia, encyclopédie libre
Minetest, équivalent libre de Mincraft → Framaminetest

Algem, outil de gestion socio-culturelle

Abuledu, suite logicielles pour les écoles

Canoprof, pour produire vos contenus pédagogique (outils Canopée)

OpenDyslexic, police qui facilite la lecture pour les dyslexiques

Scratch, pour apprendre à programmer

Moodle, plateforme d'apprentissage

OpenBoard, tableau intéractif

GeoGebra, outil pour la géométrie

GCompris, suite logiciels pour les écoles avec de nombreuses activités éducatives

## Art et Culture

Licences libres Creative Commons, pour donner si vous le souhaitez des libertés aux réutilisateurs de vos œuvres

Open Culture, ressource de pleins de contenu libre d'accès (pas toujours libres de droits)

Graphisme : Gimp, Inkscape, Krita

DAO: Scribus

Voir aussi Liberapay

## Climat, Eau, Energie & Biodiversité

##Déchets et Réemploi

Utiliser Linux pour votre vieil ordi (enfin pas trop vieux non plus, 10 ans max)

## Économie

Monnaies libres, Duniter

Liberapay, plateforme libre de rémunération type patreon ou tipee

## Habitat & Co

OpenDesktop, designs libres de mobilier

SweetHome3D, outil de plan en 3D

FreeCad, outil de CAO

## Pouvoir d’agir, démocratie & Citoyenneté

Loomio, forum doté d'un outil de vote → Framavox

"Savoir c'est pouvoir" OpenData en général : des portails par pays, région, département, communautés de communes

Guide logiciels libres pour les associations

Les framaservices permettant de s'organiser : framateam (mattermost), framaform, framadata, framagenda, framaboard, framaliste, framatalk, …

## Relation en conscience, Transition intérieure

## Santé

Outil gestion médecin

## Solidarité & Partage


## Transport et Mobilité

OpenData Mobilité

OpenStreetMap
