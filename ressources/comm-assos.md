# Communication vers les associations

## Mail pour inviter les assos et collectifs pour un stand ou un atelier

Bonjour,

Vous le savez sûrement, [Alternatiba Toulouse](https://alternatiba.eu/toulouse/) aura lieu les 23 et 24 septembre prochains. Cet événement met en avant les alternatives dans plusieurs domaines : santé, media, habitat, énergie, économie… Je coanime le [village AlterMediaTic](https://alternatiba.eu/toulouse/alternatiba-2017/thematiques/altermediatic/) qui mettra en avant les alternatives dans les domaines des media et du numérique, avec des projets tels que :

* création d'un hébergeur internet toulousain répondant à la charte des [CHATONS](http://chatons.org/), à destination des associations
* reprendre la main sur sa vie numérique :
    * démo et ateliers sur les logiciels libres et services en ligne basés sur des logiciels libres ([degooglisons-internet](http://degooglisons-internet.org/))
    * ateliers auto-défense numérique (chiffrer vos mails, reconnaître le phishing, etc.)
* plateau TV avec [TV Bruits](http://tvbruits.org/)
* atelier media indépendants
* débat sur le plateau TV "quelle éducation aux media ?"
* un atelier hackathon / maker / openbidouille pour réaliser des projets utiles pour la transition écologique et citoyenne.
    * Une ligne directrice pour le moment : profiler et trouver le moyen de mesurer, profiler et réduire la consommation des technologies numérique : soft/hard tout le monde est le bienvenu !
* communs du numérique, creative commons, partage de la connaissance
* impact du numérique sur l'environnement et la société

**Si vous, votre collectif ou votre association souhaitez tenir un stand sur le village (ou une autre thématique), proposer un atelier ou participer à ceux proposés ci-dessus, ou bien simplement laisser de la documentation faites-le nous savoir**

* par mail à la liste du village AlterMediaTic toulouse_media@alternatiba.eu,
* rejoignez-nous sur le [canal de discussion sur framateam](https://framateam.org/alternatibatlse/channels/altermediatic)
* ou [remplissez le formulaire](https://docs.google.com/forms/d/1gviJzrRcEEjTl5yvLCMPbPIMq0fJEAPKspk_8_DpVQE/viewform) (oui on sais, un google form, on n'a pas assuré la transition pour tout alternatiba encore, mais c'est en progrès)

Et si individuellement vous êtes disponible le weekend du 23-24 septembre, [vous pouvez être bénévole](https://alternatiba.eu/toulouse/alternatiba-2017/benevoles/), même pour 2h dans la journée.

Bonne journée,

## Même mail en plus court

Bonjour,

Vous le savez sûrement, **[Alternatiba Toulouse](https://alternatiba.eu/toulouse/)** aura lieu les **23 et 24 septembre** prochains. Cet événement met en avant les alternatives dans plusieurs domaines : santé, media, habitat, énergie, économie… Je coanime le [village AlterMediaTic](https://alternatiba.eu/toulouse/alternatiba-2017/thematiques/altermediatic/) qui mettra en avant les alternatives dans les domaines des media et du numérique, avec des projets tels que :

* impact du numérique sur l'environnement et la société, atelier hackathon sur la consommation d'énergie
* communs du numérique, creative commons, partage de la connaissance
* stands et ateliers reprendre la main sur sa vie numérique
* stands et ateliers media indépendants
* plateau TV avec [TV Bruits](http://tvbruits.org/)
* débat sur le plateau TV "quelle éducation aux media ?"
* création d'un hébergeur internet toulousain répondant à la charte des [CHATONS](http://chatons.org/), à destination des associations

**On aimerait inviter <asso> à tenir un stand, ou même à proposer un atelier lors de cet événement. Est-ce que ça vous intéresserait ?**

Vous pouvez nous rejoindre sur plusieurs canaux de discussion :

* par mail à la liste du village AlterMediaTic toulouse_media@alternatiba.eu,
* rejoignez-nous sur le [canal de discussion sur framateam](https://framateam.org/alternatibatlse/channels/altermediatic)
* ou [remplissez le formulaire](https://docs.google.com/forms/d/1gviJzrRcEEjTl5yvLCMPbPIMq0fJEAPKspk_8_DpVQE/viewform) (oui on sais, un google form, on n'a pas assuré la transition pour tout alternatiba encore, mais c'est en progrès)

Et si individuellement vous êtes disponible le weekend du 23-24 septembre, [vous pouvez être bénévole](https://alternatiba.eu/toulouse/alternatiba-2017/benevoles/), même pour 2h dans la journée.

Bonne journée,

## OLD

## Mail type proposé par la comm

Mesdames, Messieurs, membres du réseau associatif toulousain,

En tant que référent du village (thématique), j'ai le plaisir de convier votre association à rejoindre Alternatiba Toulouse 2017 qui se tiendra les 23 et 24 septembre et qui se prépare activement dès maintenant.

Notre prochaine réunion de village à laquelle nous vous invitons à participer se tiendra le (date) à (heure) à (lieu et adresse).

Vous pouvez communiquer avec l'ensemble de notre groupe de travail et présenter votre action grâce à l'adresse générique suivante : (adresse mail du village).

## Invitation journée publique le 22/04 par le village AlterMediaTic

Vous connaissez peut-être le **mouvement Alternatiba**, qui met en avant des alternatives écologiques et initiatives citoyennes, en lien avec le mouvement de la **transition citoyenne**. En tant que membre du village AlterMediatic, je voudrais proposer à votre association de rejoindre **Alternatiba Toulouse 2017** qui se tiendra les **23 et 24 septembre** et qui se prépare activement dès maintenant.

Vous pourrez rencontrer l'ensemble des villages Alternatiba à la journée de lancement le samedi 22 avril prochain à la Maison d'action culturelle de Chapou. Nous parlerons plus en détail de ces projets de 14h à 16h.

https://alternatiba.eu/toulouse/events/journee-de-lancement-alternatiba-2017/

À cette occasion les membres du village AlterMediaTic se sont donné l'objectif d'**augmenter les pratiques et usages des medias et outils numériques alternatifs**, et ont quelques projets en cours de réalisation :

* la création d'un hébergement web toulousain à destination des associations
* des ateliers d'analyse critique des media, d'auto-défense intellectuelle sur l'information
* des ateliers pour reprendre la main sur sa vie numérique
* un atelier hackathon / maker / openbidouille pour réaliser des projets utiles pour la transition écologique et
citoyenne. Une ligne directrice pour le moment : Profiler et trouver le moyen de mesurer, profiler et réduire la consommation des
technologies numérique : soft/hard tout le monde est le bienvenu !
* des débats et conférences autour des media et du numérique
* vos idées ?

Si vous êtes intéressé pour participer à l'un de ces projets, vous pouvez nous rejoindre à notre prochaine réunion de village fin avril / début mai, et inscrivez-vous à la liste de discussion toulouse_media alternatiba.eu.

* Les Alternatiba : https://alternatiba.eu/
* Alternatiba Toulouse : https://alternatiba.eu/toulouse/
* Village AlterMediaTic : https://alternatiba.eu/toulouse/villages-thematiques/altermediatic/
