# Revue du Web du village AlterMediaTic (janvier-février 2017)

## Éducation aux media : le Décodex et ses limites

Les Décodeurs du monde ont sorti une [série d'outils pour
l'éducation aux media](http://www.lemonde.fr/les-decodeurs/article/2017/02/01/decodex-nos-conseils-pour-verifier-les-informations-qui-circulent-en-ligne_5072954_4355770.html), ce qui pourrait constituer une ressource intéressante, si ce n'est que le Monde étant eux-même un media, sont dans une position de juge et partie, objet de la controverse qui a suivi.

En réaction, [le collectif SavoirsCom1 propose de faire de la qualité de
l'information un commun](http://www.savoirscom1.info/2017/02/controverse-decodex-information-comme-un-commun/). Arrêt sur Image a invité [Samuel Laurent du Monde, François Ruffin de Fakir et Louise Merzeau de SavoirsCom1 à débattre](http://www.arretsurimages.net/emissions/2017-02-10/Decodex-on-s-engage-dans-une-guerre-contre-les-fake-news-id9534) autour de cette question.

On retrouve Louise Merzeau dans l'émission [Du grain à moudre traite également de cette question à qui confier la vérification de l'info](https://www.franceculture.fr/emissions/du-grain-moudre/qui-confier-la-verification-de-linfo), avec également les initiateurs du projet Crosscheck, une alliance de plusieurs media français associés au Google News Lab pour vérifier les informations en ligne. Comme le dit Squeeek, on dirait que la critique des médias est de plus en plus à la mode ces jours-ci ^^.

La question d'éducation aux media se pose également [à l'école élémentaire dans un blog de Mediapart](https://blogs.mediapart.fr/jean-pierre-veran/blog/070117/education-aux-medias-et-l-information-quels-objectifs-l-ecole-elementaire) partagé par Laurent.

## Les impacts écologiques des Internets, rapport Greenpeace

La grosse information est la sortie du [rapport greenpeace](http://www.clickclean.org/france/fr/), relayé notamment par [Le Journal de l'environnement](http://www.journaldelenvironnement.net/article/l-internet-une-menace-pour-la-transition-energetique,77721)

> D’énormes quantités d’énergie sont nécessaires pour fabriquer et alimenter nos appareils et faire tourner les centres de données. Selon le rapport [Click Clean](http://www.clickclean.org/) publié [le 10 janvier 2017] par Greenpeace, le secteur informatique représente aujourd’hui environ 7 % de la consommation mondiale d’électricité.

Un autre des liens partagés par Laurent : un [très bon dessin sur Causette sur l'impact écologique des téléphones](https://www.causette.fr/le-telephone-pleure/).

Mais des solutions existent, par exemple [une distribution linux ultra légère pour votre ordinausore](http://www.01net.com/actualites/recyclez-votre-vieux-pc-avec-pixel-de-raspberry-pi-1095993.html), une "variante ultra-légère de Debian Linux". Reporterre publie un [manuel des bonnes pratiques sur Internet](https://reporterre.net/Petit-manuel-des-bonnes-pratiques-ecolos-sur-Internet), notamment ne pas mettre en copie tout le monde.

## "Civic Tech", ou "Civic Business" ?

Les "Civic Tech", ou les technologies numériques au service de la démocratie à travers un [projet pour améliorer la démocratisation du
numérique](https://visions-fr.consider.it), pendant plusieurs associations du libre (April, Regards Citoyens) [revendiquent l'obligation d'utiliser des logiciels libres dans la consultation en ligne de citoyens](https://www.nextinpact.com/news/102895-consultations-en-ligne-citoyens-logiciel-libre-obligatoire.htm).

Regards Citoyens [critique les "Civic Tech" à la française](http://www.regardscitoyens.org/civic-tech-ou-civic-business-le-numerique-ne-pourra-pas-aider-la-democratie-sans-en-adopter-les-fondements/) derrière lesquelles se cache plus du "Civic Business", Valentin Chaput de Open Source Politics [explique très bien les limites d'utiliser des outils propriétaires](https://medium.com/open-source-politics/la-civic-tech-fran%C3%A7aise-risque-de-se-d%C3%A9tourner-de-la-cr%C3%A9ation-des-biens-communs-num%C3%A9riques-dont-9ebcf5c55c2e#.ayjvj7npj) pour une gouvernance ouverte.

Heureusement il y a des contre-exemples, ici une [cartographie de la corruption en France](http://www.lemonde.fr/chronique-des-communs/article/2017/02/11/une-carte-collaborative-de-la-corruption-en-france_5078252_5049504.html) en OpenData, une vraie illustration des data au service des citoyens.

## En bref

L'April a 20 ans ! [Interview de Fréd couchet de l'April sur un blog de
ZDNet](http://www.zdnet.fr/blogs/l-esprit-libre/l-april-a-20-ans-et-toutes-ses-dents-pour-defendre-le-logiciel-libre-39848430.htm).

**Benjamin Bayart** de la FFDN et la scop [SCANI](http://www.scani.fr/) parlent de l'accès à Internet en milieu rural dans [l'émission les nouvelles vagues sur France Culture](https://www.franceculture.fr/emissions/les-nouvelles-vagues/le-village-35-dans-les-zones-blanches) : zones blanches, fibre, wifi mesh, mais surtout des habitants qui s'organisent localement.

Le [THSF](https://www.thsf.net/), rencontre de hackers et d'artistes à Toulouse, aura lieu du 25 au 28 mai prochain, et a ouvert [son appel à participation](https://www.thsf.net/index.html#cfp_french).
