# Outils numériques de communication et de collaboration

Liste des [outils Altrenatiba](https://annuel.framapad.org/p/Outils-alternatiba)

## Communication

[Site web de toutes les Alternatiba](https://alternatiba.eu/).

Sur le [site web Alternatiba Toulouse](http://alternatiba.eu/toulouse/), la [page AlterMediaTIC](https://alternatiba.eu/toulouse/villages-thematiques/numerique-et-media/).

## Collaboration

Cloud

Forums

Chat / discussion : Framateam [Alternatiba toulouse](https://framateam.org/alternatibatlse/), et [salon dédié au village AlterMediaTIC](https://framateam.org/alternatibatlse/channels/altermediatic).


Éditer à plusieurs : [pads dans le groupe AlterMediaTIC Mypads](https://mypads.framapad.org/mypads/?/mypads/group/altermediatic-toulouse-deatm79d/view)

Gérer du code source, de la documentation et des tickets : [Groupe Alternatiba sur framagit](http://framagit.org/altermediatic/)
