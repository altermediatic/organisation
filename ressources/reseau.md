# Associations et collectifs

## Présence confirmée

* Framasoft (Pouhiou au courant) → Manue → conf de pouhiou + stand
* Inform'action → Manue → table de presse + atelier media indépendants & démocratie
* Le PIC (déjà rencontrés pour le projet Chatons) → Tom, Manue → Atelier FAQ, asso hébergeur de chatons
* TVBruit (premier mail envoyé) → Manue → stand + plateau TV + débat "quelle éducation aux media"
* Démosphère → Manue
* Tetaneutral → Clément → stand, connexion Internet
* Pick-A-Joy → Squeeek → stand, conf

## Réponse positive, orga en cours

* Tetalab → Clément, Manue → en cours, atelier bidouille, contact avec Fabien L.

## En attente de réponse

* Les Communs → Manue : fait
* Liste OSM (premier mail envoyé) → Manue
* Toulibre (premier mail envoyé) → Manue, Guilhem
* Wikimedia (premier mail envoyé) → Manue
* IATAA (contacté le 24/06) → Manue
* KaleidosFilm  (contacté le 24/06) → Manue
* Nuit Debout notamment commission comm' & Tech → Manue
* Media Commun, porteurs de Onde Courtes (contacté le 11/07)→ Manue

## À contacter

* Arboré'Sign → Manue
* Artilect → Clément
* BadKids → Manue
* Café Bricoles ?
* Civic Tech et [Sud-ouest Civic Tech](http://www.so-civictech.com/)
* Combustible → Tom
* CryptoToulouse → Manue
* Friture (ex media) → Manue (voir si toujours actif)
* Les meetups ? on se pose la question s'ils seront réellement intéressés
* Open Bidouille Camp → Manue et Alima
* OpenData ?
* Canal Sud
* FMR
* Campus FM
* MediaCités → Julien
* Radiom → Manue
* Transparence (Festival Resistance)
* Caméra au poing

## Depuis formulaire Alternatiba

* PicoJoule → énergie
* 6/15/2017 15:19:22	CIDES	UNE ASSOCIATION
* 6/20/2017 17:21:23	media commun
